EZCraft = {}
local EZCraft_itemList = nil;
local EZCraft_menuList = nil;
local EZCraft_slotNames = {};
EZCraft_slotNames[GameData.TradeSkills.APOTHECARY] = { "ApothecaryWindowContainerSlot", "ApothecaryWindowDeterminentSlot", "ApothecaryWindowResource1Slot", "ApothecaryWindowResource2Slot", "ApothecaryWindowResource3Slot" };
EZCraft_slotNames[GameData.TradeSkills.TALISMAN] = { "TalismanMakingWindowContainerSlot", "TalismanMakingWindowDeterminentSlot", "TalismanMakingWindowGoldSlot", "TalismanMakingWindowCuriosSlot", "TalismanMakingWindowMagicSlot" };
EZCraft_slotNames[GameData.TradeSkills.CULTIVATION] = { "CultivationWindowSinglePlotSeedSpore", "CultivationWindowSinglePlotSoil", "CultivationWindowSinglePlotWater", "CultivationWindowSinglePlotNutrient" };


--
--  Helper function to get the current version specified in the .mod-file
--
function EZCraft.getVersion()
  local mods = ModulesGetData();
  for modIndex, modData in ipairs( mods ) do
    if( modData.name == "EZCraft" ) then
      return modData.version;
    end
  end
  return "";
end

--
--  Initialize of the program
--
function EZCraft.OnInitialize()
  EA_ChatWindow.Print(towstring("EZCraft " .. EZCraft.getVersion() .. " Loaded."));
  WindowRegisterEventHandler( "EZCraftWindow", SystemData.Events.L_BUTTON_DOWN_PROCESSED, "EZCraft.OnLButtonDown" );
end

--
--  Handles all leftclicks ingame, check to see if the window u clicked is specified in EZCraft_slotNames
--
function EZCraft.OnLButtonDown()
  for craftSkill, slotNames in pairs(EZCraft_slotNames) do
    for _, slotName in pairs(slotNames) do
      if( SystemData.MouseOverWindow.name == slotName ) then
        EZCraft.showMenu( craftSkill, WindowGetId(SystemData.MouseOverWindow.name) );
      end
    end
  end
end

--
--  Handles mouseover contextmenu for tooltips
--
function EZCraft.OnUpdate()
  local id = WindowGetId(SystemData.MouseOverWindow.name);
  if( string.sub(SystemData.MouseOverWindow.name,1,21) == "EA_Window_ContextMenu" and EZCraft_itemList ~= nil and EZCraft_menuList[id] ~= nil and EZCraft_menuList[id].menuindex ~= 0 and EA_Window_ContextMenu.activeWindow == "EZCraft_contextMenu" ) then
    id = EZCraft_menuList[id].menuindex;
	Tooltips.CreateItemTooltip( EZCraft_itemList[id].item, SystemData.MouseOverWindow.name );
  end
end

--
--  Handler for click on the contextmenu
--
function EZCraft.contextMenuOnClick()
  local id = WindowGetId(SystemData.MouseOverWindow.name);
  id = EZCraft_menuList[id].menuindex
  local slotorplot = nil;
  
  if( EZCraft_itemList[id].tradeSkill == GameData.TradeSkills.CULTIVATION ) then
    slotorplot = GameData.Player.Cultivation.CurrentPlot;
  else
    if( EZCraft_itemList[id].tradeSkill == GameData.TradeSkills.APOTHECARY and ApothecaryWindow.craftingData[ EZCraft_itemList[id].slot ] ~= nil and ApothecaryWindow.craftingData[ EZCraft_itemList[id].slot ].sourceSlot ~= nil ) then
      ApothecaryWindow.RemoveVirtualItemSlot( EZCraft_itemList[id].slot );
      RemoveCraftingItem( GameData.TradeSkills.APOTHECARY, ApothecaryWindow.craftingData[ EZCraft_itemList[id].slot ].sourceSlot );
    elseif( EZCraft_itemList[id].tradeSkill == GameData.TradeSkills.TALISMAN and TalismanMakingWindow.craftingData[ EZCraft_itemList[id].slot ] ~= nil and TalismanMakingWindow.craftingData[ EZCraft_itemList[id].slot ].sourceSlot ~= nil ) then
      TalismanMakingWindow.RemoveVirtualItemSlot( EZCraft_itemList[id].slot );
      RemoveCraftingItem( GameData.TradeSkills.TALISMAN, TalismanMakingWindow.craftingData[ EZCraft_itemList[id].slot ].sourceSlot );
    end
    slotorplot = EZCraft_itemList[id].slot;
  end

  EZCraft.AddItem( EZCraft_itemList[id].tradeSkill, slotorplot, EZCraft_itemList[id].invIndex );
end

--
--  function that print a contextmenu of items that can go into TradeSkill and Slot
--
function EZCraft.showMenu(TradeSkill,Slot)
  local items = DataUtils.GetItems();
  local i = 1;
  EZCraft_itemList = nil;
  EZCraft_menuList = {};
  for index,itemData in pairs(items) do 
    if( EZCraft.ItemIsAllowedInSlot( TradeSkill, itemData, Slot ) ) then
      if( EZCraft_itemList == nil) then EZCraft_itemList = {} end
      EZCraft_itemList[i] = {invIndex=index,item=itemData,tradeSkill=TradeSkill,slot=Slot};
      i=i+1;
    end
  end

  if( EZCraft_itemList ~= nil ) then
    table.sort(EZCraft_itemList, function(a,b) return a["item"].craftingSkillRequirement<b["item"].craftingSkillRequirement end)

    EA_Window_ContextMenu.CreateContextMenu("EZCraft_contextMenu")
	local lvl = 0;
	i = 1;
	j = 1;
    for index,data in pairs(EZCraft_itemList) do
		   if lvl == 0 then
				lvl = data.item.craftingSkillRequirement;
				EA_Window_ContextMenu.AddMenuItem(L"---Level "..lvl..L"---", EZCraft.contextMenuOnClick, true, true)
				EZCraft_menuList[i] = {menuindex=0};
				i=i+1;
				j=j+1;
		   elseif lvl ~= data.item.craftingSkillRequirement then
		        lvl = data.item.craftingSkillRequirement;
				EA_Window_ContextMenu.AddMenuDivider( 1 );
				EZCraft_menuList[i] = {menuindex=0};
				i=i+1;
				EA_Window_ContextMenu.AddMenuItem(L"---Level "..lvl..L"---", EZCraft.contextMenuOnClick, true, true)
				EZCraft_menuList[i] = {menuindex=0};
				i=i+1;
				j=j+1
		   end
		   
		   --Current Style
           EA_Window_ContextMenu.AddMenuItem(data.item.stackCount..L" x "..data.item.name, EZCraft.contextMenuOnClick, false, true);
		   
		   --Test code for trying to colourise the items... and input appreciated on how to get it working properly...
		   --ButtonSetTextColor("EA_Window_ContextMenu1DefaultItem"..j, 1, 255, 0, 0)
		   --Possible New Style using windows (needs work but the colour concept is right)
		   --CreateWindowFromTemplate("EZCraftContextMenu"..index, "ChatContextMenuItemCheckBox", "Root")
		   --LabelSetText("EZCraftContextMenu"..index.."CheckBoxLabel", data.item.stackCount..L" x "..data.item.name  )
		   --LabelSetTextColor("EZCraftContextMenu"..index.."CheckBoxLabel", 255,0,0)
		   --WindowRegisterCoreEventHandler("QueueQueuer_GUI_Context1", "OnLButtonUp", "EZCraft.contextMenuOnClick")
		   --WindowSetId("EZCraftContextMenu"..index, index)
		   --WindowSetShowing("EZCraftContextMenu"..index, false)
		   --EA_Window_ContextMenu.AddUserDefinedMenuItem("EZCraftContextMenu"..index,1)
			
		   
		   EZCraft_menuList[i] = {menuindex=index};
		   i=i+1;
		   j=j+1
	end
    EA_Window_ContextMenu.Finalize();
	
  end
end

--
--  This function checks if items is allowed in slot
--
function EZCraft.ItemIsAllowedInSlot( TradeSkill, itemData, Slot )
  if( TradeSkill == GameData.TradeSkills.APOTHECARY ) then
    	
    if (Slot == ApothecaryWindow.SLOT_CONTAINER) then
		local craftingTypes, resourceType = CraftingSystem.GetCraftingData( itemData )
		if resourceType == GameData.CraftingItemType.CONTAINER or resourceType == GameData.CraftingItemType.CONTAINER_DYE then
			return true;
        end
	-- OK DONT ASK WHY.... next compare should be (Slot == ApothecaryWindow.SLOT_DETERMINANT) but that doesnt work and dont know why so its Slot == 1
	elseif (Slot == 1) and (ApothecaryWindow.productType == GameData.CraftingItemType.CONTAINER_DYE) then
		local craftingTypes, resourceType = CraftingSystem.GetCraftingData( itemData );
		if resourceType == GameData.CraftingItemType.PIGMENT then
			return true;
        end
    elseif (Slot == ApothecaryWindow.SLOT_INGREDIENT1) and (ApothecaryWindow.productType == GameData.CraftingItemType.CONTAINER_DYE) then
		local craftingTypes, resourceType = CraftingSystem.GetCraftingData( itemData );
		if resourceType == GameData.CraftingItemType.FIXER then
			return true;
        end
	else
      return ApothecaryWindow.ItemIsAllowedInSlot( itemData, Slot );
	end
  elseif( TradeSkill == GameData.TradeSkills.TALISMAN ) then
    return TalismanMakingWindow.ItemIsAllowedInSlot( itemData, Slot );
  elseif( TradeSkill == GameData.TradeSkills.CULTIVATION ) then
    -- If current clicked slot is greater than or equal to the active stage we are on
    if( Slot >= CultivationWindow.plot[GameData.Player.Cultivation.CurrentPlot].StageNum + 1 ) then
      -- If current item got correct cultivationType for current active stage, also check fore spore on stage EMPTY
      if( itemData.cultivationType == Slot or Slot == GameData.CultivationStage.EMPTY + 1 and itemData.cultivationType == GameData.CultivationTypes.SPORE ) then
        return true;
      end
    end
  end
  return false;
end

--
--  This function adds items to the diffrent slots
--
function EZCraft.AddItem( TradeSkill, SlotOrPlot, invIndex )
  if( TradeSkill == GameData.TradeSkills.APOTHECARY ) then
    ApothecaryWindow.AddItem( invIndex, SlotOrPlot );
  elseif( TradeSkill == GameData.TradeSkills.TALISMAN ) then
    TalismanMakingWindow.AddItem( invIndex, SlotOrPlot );
  elseif( TradeSkill == GameData.TradeSkills.CULTIVATION ) then
    AddCraftingItem( TradeSkill, SlotOrPlot, invIndex );
  end
end
