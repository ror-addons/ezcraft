<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="EZCraft" version="1.3.2" date="4/09/2008" >

		<Author name="WarWolf" email="warwolf.667@gmail.com" />
		<Description text="Makes crafting faster by providing list for craftitems." />

	    <Dependencies>
	    	<Dependency name="EA_ChatWindow" />
			<Dependency name="EA_CraftingSystem" />
	    </Dependencies>

		<Files>
			<File name="EZCraft.xml" />
		</Files>
		
		<OnInitialize>
			<CreateWindow name="EZCraftWindow" show="false" />
			<CallFunction name="EZCraft.OnInitialize" />
		</OnInitialize>
		<OnUpdate>
		    <CallFunction name="EZCraft.OnUpdate" />
		</OnUpdate>
	</UiMod>
</ModuleFile>
